package ru.geekbrains;

import java.util.Arrays;
import java.util.stream.IntStream;

import static java.lang.Math.max;

public class Main {

    public static int decision = 0;

    public static void main(String[] args) {
        int[][] arr1 = {
            {1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1},
            {1, 1, 0, 1, 1, 1},
            {1, 1, 1, 0, 1, 1},
            {1, 1, 1, 1, 0, 0},
            {1, 1, 1, 1, 1, 0}
        };
        //task1(arr1);

        System.out.println();

        int[] a = {1, 2, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] b = {1, 2, 2, 3, 10, 7, 8, 11, 9};
        //task2(a, b);

        //task3();
    }

    /**
     * Задача 1
     * *Количество маршрутов с препятствиями. Реализовать чтение массива с препятствием и нахождение
     * количества маршрутов.
     */
    public static void task1(int[][] map) {
        int i, j, n = map.length, m = map[0].length;
        int[][] arr = new int[n][m];
        for (j = 0; j < m; j++) {
            arr[0][j] = (map[0][j] == 1) ? 1 : 0;
        }
        for (i = 1; i < n; i++) {
            arr[i][0] = (map[i][0] == 1) ? 1 : 0;
            for (j = 1; j < m; j++) {
                if (map[i][j] == 0) arr[i][j] = 0;
                else arr[i][j] = arr[i - 1][j] + arr[i][j - 1];
            }
        }
        printArray(arr);
    }

    /**
     * Задача 2
     * Решить задачу о нахождении длины максимальной последовательности с помощью матрицы.
     */
    public static void task2(int[] a, int[] b) {
        final int matrixWidth = a.length + 2;
        final int matrixHeight = b.length + 2;
        int[][] matrix = new int[matrixHeight][matrixWidth];
        for (int i = 2; i < matrixWidth; i++) matrix[0][i] = a[i - 2];
        for (int i = 2; i < matrixHeight; i++) matrix[i][0] = b[i - 2];
        for (int i = 2; i < matrixHeight; i++) {
            for (int j = 2; j < matrixWidth; j++) {
                if (a[j - 2] == b[i - 2]) matrix[i][j] = matrix[i - 1][j - 1] + 1;
                else matrix[i][j] = max(matrix[i - 1][j], matrix[i][j - 1]);
            }
        }
        printArray(matrix);
    }

    public static void printArray(int[][] arr) {
        IntStream stream = Arrays.stream(arr).flatMapToInt(Arrays::stream);
        int max = stream.max().getAsInt();
        int maxLen = String.valueOf(max).length();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                int elLen = String.valueOf(arr[i][j]).length();
                for (int k = elLen; k < maxLen; k++) System.out.print(" ");
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * Задача 3
     * ***Требуется обойти конём шахматную доску размером NxM, пройдя через все поля доски по одному разу.
     * Здесь алгоритм решения такой же как и в задаче о 8 ферзях. Разница только в проверке положения коня.
     */
    public static void task3() {
        int[][] board = new int[4][4];
        bewareOfTheHorse(0, 0, board, 1); // находит первое подходящее решение
        if (boardHasEmptyCells(board)) {
            System.out.println("Решения не существует");
        } else {
            System.out.println("Решение:");
            printArray(board);
        }
        // bewareOfTheHorseAll(0, 0, new int[5][5], 1); // находит все возможные решения, для 8х8 лучше не запускать
    }

    /**
     * Находит первое подходящее решение и на этом останавливается.
     */
    public static void bewareOfTheHorse(int y, int x, int[][] b, int r) {
        int n = b.length;
        int m = b[0].length;
        if (r > n * m) return;
        int[][] board = b;
        board[y][x] = r;
        int[][] horseRuns = { {-2, 1}, {-1, 2}, {1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1} };
        for (int k = 0 ; k < horseRuns.length ; k++ ) {
            int c = y + horseRuns[k][0];
            int e = x + horseRuns[k][1];
            if (c < 0 || c > n - 1 || e < 0 || e > m - 1) continue;
            if (board[c][e] != 0) continue;
            bewareOfTheHorse(c, e, board, r + 1);
            if (boardHasEmptyCells(board)) {
                board[c][e] = 0;
            }
        }
    }

    /**
     * Находит все возможные решения. Количество решений зависит от точки старта.
     * Для 5х5 при старте с точки 0 0 существует 304 решения.
     * Для 8х8 при старте с точки 0 0 я так и не дождался подсчета всех возможных решений.
     */
    public static void bewareOfTheHorseAll(int y, int x, int[][] b, int r) {
        int n = b.length;
        int m = b[0].length;
        if (r > n * m) return;
        int[][] board = arrayCopy(b);
        board[y][x] = r;
        if (!boardHasEmptyCells(board)) {
            System.out.println("Решение №:" + ++decision);
            printArray(board);
            System.out.println();
            return;
        }
        int[][] horseRuns = { {-2, 1}, {-1, 2}, {1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1} };
        for (int k = 0 ; k < horseRuns.length ; k++ ) {
            int c = y + horseRuns[k][0];
            int e = x + horseRuns[k][1];
            if (c < 0 || c > n - 1 || e < 0 || e > m - 1) continue;
            if (board[c][e] != 0) continue;
            bewareOfTheHorseAll(c, e, board, r + 1);
        }
    }

    private static int[][] arrayCopy(int[][] arr) {
        int[][] newArr = new int[arr.length][arr[0].length];
        for(int i = 0; i < arr.length; i++) {
            for(int j = 0; j < arr[i].length; j++) {
                newArr[i][j] = arr[i][j];
            }
        }
        return newArr;
    }

    public static boolean boardHasEmptyCells(int[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == 0) return true;
            }
        }
        return false;
    }
}
